<?php
require_once('worker/trainingDBManager.php');

/**
 * Script trainingManager
 *
 * Récupération des requetes en relation avec les sessions d'entrainment
 *
 * @version 1.0
 * @author Pasquier Louis
 * @project Training Manager
 */

session_start();

if (isset($_SERVER['REQUEST_METHOD'])) {

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        if($_GET['action'] == "getSessions") {

            //control de session
            if (!isset($_SESSION['connectedUser'])) {
                http_response_code(401);
                echo '{"message":"Erreur lors de la recuperation de lutilisateur"}';
                return;
            }

            $trainingBD = new TrainingDBManager();
            http_response_code(200);
            echo $trainingBD->GetSessions();
            return;
        }

        if($_GET['action'] == "getExerciceSemaine") {

            //control de session
            if (!isset($_SESSION['connectedUser'])) {
                http_response_code(401);
                return;
            }

            $trainingBD = new TrainingDBManager();
            http_response_code(200);
            echo $trainingBD->GetExerciceSemaine();
            return;
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        if($_POST['action'] == "addSession") {

            //control de session
            if (!isset($_SESSION['connectedUser'])) {
                http_response_code(401);
                echo '{"message":"Erreur lors de l ajout"}';
                return;
            }

            if (isset($_POST['date'])) {
                $date = htmlentities($_POST['date']);

                $trainingBD = new TrainingDBManager();
                http_response_code(200);
                $trainingBD->AddTrainingSession($date);
                echo '{"message":"Session ajoutée"}';
                return;
            }
            http_response_code(400);
            echo '{"message":"Erreur lors de l ajout"}';
            return;
        }

        if($_POST['action'] == "addExerciceSemaine") {

            //control de session
            if (!isset($_SESSION['connectedUser'])) {
                http_response_code(401);
                echo '{"message":"Erreur lors de l ajout"}';
                return;
            }

            if (isset($_POST['exercice']) and isset($_POST['semaine']) and isset($_POST['poidgauche']) and isset($_POST['poiddroite'])) {
                $exercice = htmlentities($_POST['exercice']);
                $semaine = htmlentities($_POST['semaine']);
                $poidgauche = htmlentities($_POST['poidgauche']);
                $poiddroite = htmlentities($_POST['poiddroite']);

                $trainingBD = new TrainingDBManager();

                if ( $trainingBD->AddExerciceSemaine($_POST['exercice'], $_POST['semaine'], $_POST['poidgauche'], $_POST['poiddroite']) ) {
                    http_response_code(200);
                    echo '{"message":"Session ajoutée"}';
                    return;
                }
                http_response_code(404);
                echo '{"message":"Erreur lors de l ajout"}';
                return;
            }
            http_response_code(400);
            echo '{"message":"Erreur lors de l ajout"}';
            return;
        }

    }
}

?>