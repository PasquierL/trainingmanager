<?php
require_once('connection/Connexion.php');

/**
 * Classe LoginDBManager
 *
 * Gestion des utilisateurs, connexion, deconnexion, est administrateur
 *
 * @version 1.0
 * @author Pasquier Louis
 * @project Training Manager
 */
class LoginDBManager
{
    /**
     * Requete de connexion pour un utilisateur
     * Le mot de passe est haché dans la db, pour vérifier il faut utiliser password_verify()
     *
     * @param string $username nom d'utilisateur
     * @param string $password mot de passe
     * @return boolean
     */
    public function Connect($username, $password) {

        $query = Connexion::getInstance()->SelectQuery('SELECT Password, PK_User from t_user where Username = :Username;',
            array('Username' => $username));

        foreach($query as $data) {
            if (password_verify($password, $data['Password'])) {
                $_SESSION['connectedUser'] = $data['PK_User'];
                return true;
            }
        }
        return false;
    }

    /**
     * Vérifie si l'utilisateur connecté est un administrateur
     *
     * @return string résultat en JSON
     */
    public function IsAdmin() {

        $query = Connexion::getInstance()->SelectQuery('SELECT isAdmin from t_user where PK_User = :PK_User',
            array('PK_User' => $_SESSION['connectedUser']));

        foreach($query as $data) {
            if ($data['isAdmin'] == 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * Ajoute un utilisateur dans la DB, le mot de passe est haché
     * avec password_hash(). On vérifie avec une requete SELECT si le
     * user à été ajouté
     *
     * @param string $username nom d'utilisateur
     * @param string $password mot de passe
     * @param int $isAdmin 0 ou 1
     * @return string résultat en JSON
     */
    public function Add($username, $password, $isAdmin) {

        //check if user exist
        $query = Connexion::getInstance()->SelectQuery('SELECT Username from t_user where Username = :Username', array('Username' => $username));
        foreach($query as $data) {
            if ($data['Username'] == $username) {
                return false;
            }
        }

        //hash
        $hashPassword = password_hash($password, PASSWORD_DEFAULT);

        //add
        Connexion::getInstance()->ExecuteQuery(
            'INSERT INTO t_user (Username, Password, isAdmin) 
            VALUES (:Username, :Password, :isAdmin)',
            array('Username' => $username, 'Password' => $hashPassword, 'isAdmin' => $isAdmin));

        //check if user was added
        $query = Connexion::getInstance()->SelectQuery('SELECT Username from t_user where Username = :Username', array('Username' => $username));
        foreach($query as $data) {
            if ($data['Username'] == $username) {
                return true;
            }
        }

        return false;
    }

}
?>