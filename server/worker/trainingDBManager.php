<?php
require_once('connection/Connexion.php');

/**
 * Classe TrainingDBManager
 *
 * Gestion des sessisons d'entrainement, ajout et lecture
 *
 * @version 1.0
 * @author Pasquier Louis
 * @project Training Manager
 */
class TrainingDBManager
{

    /**
     * Permet d'obtenir toutes les sessions d'entrainements
     *
     * @return false|string le résultat de la requete encodé
     */
    public function GetSessions() {

        //get all training sessions from the connected user
        $result = Connexion::getInstance()->SelectQuery('SELECT Nom, PoidDroite, PoidGauche, Date, FK_User
            FROM tr_exercice_semaine
            inner join t_exercice te on tr_exercice_semaine.FK_Exercice = te.PK_Exercice
            inner join t_trainingsession tt on tr_exercice_semaine.FK_TrainingSession = tt.PK_TrainingSession
            where FK_User = :FK_User
            order by Date', array('FK_User' => $_SESSION['connectedUser']));

        return json_encode($result);
    }

    /**
     * Permet d'obtenir tous les exercices par semaines,
     * utilisé pour générer le graphique
     *
     * @return false|string le résultat de la requete encodé
     */
    public function GetExerciceSemaine() {

        //get all training sessions from the connected user
        $result = Connexion::getInstance()->SelectQuery('SELECT Nom, PoidDroite, PoidGauche, Semaine, FK_User
            FROM tr_exercice_semaine
            inner join t_exercice te on tr_exercice_semaine.FK_Exercice = te.PK_Exercice
            inner join t_trainingsession tt on tr_exercice_semaine.FK_TrainingSession = tt.PK_TrainingSession
            inner join t_semaine ts on tr_exercice_semaine.FK_Semaine = ts.PK_Semaine
            where FK_User = :FK_User
            order by Semaine', array('FK_User' => $_SESSION['connectedUser']));

        return json_encode($result);
    }

    /**
     * Permet d'ajouter un exercice réalisé une certaine semaine avec les poids soulevés
     *
     * @param string $exercice nom de l'exercice à ajouter
     * @param string $semaine semaine d'entrainement ou l'exercice à été realisé
     * @param string $poidgauche poid soulevé à gauche
     * @param string $poiddroite poid soulevé à droite
     * @return string le resultat en JSON
     */
    public function AddExerciceSemaine($exercice, $semaine, $poidgauche, $poiddroite) {

        //get exercice pk
        $exerciceQuery = Connexion::getInstance()->SelectQuery('SELECT PK_Exercice FROM t_exercice where Nom = :Exercice;', array('Exercice' => $exercice));
        $pkexercice = 0;
        foreach ($exerciceQuery as $data) {
            $pkexercice = $data['PK_Exercice'];
        }
        if ($pkexercice == 0) return false;

        //get training session
        $latestsession = Connexion::getInstance()->SelectQuery('SELECT PK_TrainingSession FROM t_trainingsession order by PK_TrainingSession desc limit 1 ', null);
        $pklatestsession = 1;
        foreach ($latestsession as $data) {
            $pklatestsession = $data['PK_TrainingSession'];
        }

        //add
        Connexion::getInstance()->ExecuteQuery('INSERT INTO tr_exercice_semaine (FK_Exercice, FK_Semaine, FK_TrainingSession, PoidGauche, PoidDroite)
            VALUES (:PK_Exercice, :Semaine, :PK_LatestSession, :PoidGauche, :PoidDroite)',
            array('PK_Exercice' => $pkexercice, 'Semaine' => $semaine, 'PK_LatestSession' => $pklatestsession, 'PoidGauche' => $poidgauche, 'PoidDroite' => $poiddroite));

        return true;

    }

    /**
     * Ajout d'une session d'entrainement
     *
     * @param date $date Date de la session d'entrainement
     * @return string le resultat en JSON
     */
    public function AddTrainingSession($date) {
        //add
        Connexion::getInstance()->ExecuteQuery('INSERT INTO t_trainingsession (Date, FK_User) 
            VALUES (:Date, :FK_User)',
            array('Date' => $date, 'FK_User' => $_SESSION['connectedUser']));
    }



}