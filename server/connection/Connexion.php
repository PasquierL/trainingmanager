<?php
include_once('configConnexion.php');

/**
 * Classe Connexion
 *
 * Connexion et envoi de requetes à la DB
 *
 * @version 1.0
 * @author Pasquier Louis
 * @project Training Manager
 */
class Connexion {

	/**
	 * @var PDO $pdo objet permettant la connexion à la DB et l'execution de requete
	 */
	private $pdo;

	/**
	 * @var null $_instance unique instance de la classe
	 */
	private static $_instance = null;


	/**
	 * Constructeur de la classe, cette classe est un singleton
	 * donc le constructeur est "private"
	 */
	private function __construct() {
		try {
			$this->pdo = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		} catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
	}

	/**
	 * Permet de retourner une instance de la classe
	 *
	 * @return Connexion|null l'instance de la classe
	 */
	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new Connexion();
		}
		return self::$_instance;
	}

	/**
	 * Fonction permettant d'exécuter un select dans MySQL.
	 * A utiliser pour les SELECT.
	 *
	 * @param String $query Requête à exécuter
	 * @param String $params Requête à exécuter
	 * @return array|false Résultat de la requete
	 */
	public function SelectQuery($query, $params) {
		try {
			$queryPrepared = $this->pdo->prepare($query);
			$queryPrepared->execute($params);
			return $queryPrepared->fetchAll();
		} catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
	}

	/**
	 * Fonction permettant d'exécuter une requête MySQL.
	 * A utiliser pour les UPDATE, DELETE, INSERT.
	 *
	 * @param String $query. Requête à exécuter.
	 * @return true si la requête a été executée
	 */
	public function ExecuteQuery($query, $params) {
		try {
			$queryPrepared = $this->pdo->prepare($query);
			$queryRes = $queryPrepared->execute($params);
			return $queryRes;
		} catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
	}

}

?>
