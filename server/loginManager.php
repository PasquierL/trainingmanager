<?php
require_once('worker/loginDBManager.php');

/**
 * Script loginManager
 *
 * Récupération des requetes de login
 *
 * @version 1.0
 * @author Pasquier Louis
 * @project Training Manager
 */

session_start();

if (isset($_SERVER['REQUEST_METHOD'])) {

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        //on se deconnecte
        if ($_GET['action'] == "disconnect") {

            // effacer la variable de session
            if (isset($_SESSION['connectedUser'])) {
                unset($_SESSION['connectedUser']);
            }
            // supprime la session
            session_destroy();
            http_response_code(200);
            echo '{"message":"deconnexion"}';
            return;
        }

        //on demande si le user est admin
        if ($_GET['action'] == "isAdmin") {
            if (isset($_SESSION['connectedUser'])) {
                $loginBD = new LoginDBManager();
                if ($loginBD->IsAdmin()) {
                    http_response_code(200);
                    echo '{ "result" : "true" }';
                    return;
                }
                http_response_code(200);
                echo '{ "result" : "false" }';
                return;
            }
            http_response_code(400);
            echo '{ "result" : "false" }';
            return;
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        //on se connecte
        if ($_POST['action'] == "connect") {
            if (isset($_POST['username']) and isset($_POST['password']))
            {
                $username = htmlentities($_POST['username']);
                $password = htmlentities($_POST['password']);

                $loginBD = new LoginDBManager();
                if ($loginBD->Connect($username, $password)) {
                    http_response_code(200);
                    echo '{ "result" : "true" }';
                    return;
                }
                http_response_code(500);
                echo '{ "result" : "false" }';
                return;
            }
            http_response_code(400);
            echo '{ "result" : "false" }';
            return;
        }

        //register
        if($_POST['action'] == "add") {
            if (isset($_POST['username']) and isset($_POST['password']) and isset($_POST['isAdmin']))
            {
                $username = htmlentities($_POST['username']);
                $password = htmlentities($_POST['password']);
                $isAdmin = htmlentities($_POST['isAdmin']);

                $loginBD = new LoginDBManager();
                if ($loginBD->Add($_POST['username'], $_POST['password'], $_POST['isAdmin'])) {
                    http_response_code(200);
                    echo '{"message":"utilisateur ajouté"}';
                    return;
                }
                http_response_code(500);
                echo '{"message":"erreur lors de l ajout"}';
                return;
            }
            http_response_code(400);
            echo '{"message":"erreur lors de l ajout"}';
            return;
        }
    }

}
?>