/**
 * @file Permet de charger les vues
 * @author Louis Pasquier
 */
class ServiceJs {
    constructor() {}

    /**
     * Toutes les erreures sont gérée par cette méthode
     *
     * @param httpErrorCallbackFn
     */
    centraliserErreurHttp(httpErrorCallbackFn) {
        $.ajaxSetup({
            error: function (xhr, exception) {
                let msg;
                if (xhr.status === 0) {
                    msg = "Pas d'accès à la ressource serveur demandée ! ";
                } else if (xhr.status === 404) {
                    msg = "Page demandée non trouvée [404] !";
                } else if (xhr.status === 500) {
                    msg = "Erreur interne sur le serveur [500] !";
                } else if (exception === "parsererror") {
                    msg = "Erreur de parcours dans le JSON !" + exception.response;
                } else if (exception === "timeout") {
                    msg = "Erreur de délai dépassé [Time out] !";
                } else if (exception === "abort") {
                    msg = "Requête Ajax stoppée !";
                } else {
                    msg = "Erreur inconnue : \n" + xhr.responseText;
                }
                httpErrorCallbackFn(msg);
            },
        });
    }

    /**
     * Permet de charger le coeur de la page
     *
     * @param vue nom de la vue à charger
     * @param callback le callback, si il y en a un
     */
    chargerVue(vue, callback) {

        // charger la vue demandee
        $("#view").load("views/" + vue + ".html", function () {

            // si une fonction de callback est spécifiée, on l'appelle ici
            if (typeof callback !== "undefined") {
                callback();
            }

        });
    }

    /**
     * Permet de charger le menu de navigation de la page (différent si admin)
     *
     * @param vue nom de la vue à charger
     * @param callback le callback, si il y en a un
     */
    chargerMenu(vue, callback) {

        // charger la vue demandee
        $("#navigationMenu").load("views/navigationMenu/"+ vue +".html", function () {

            // si une fonction de callback est spécifiée, on l'appelle ici
            if (typeof callback !== "undefined") {
                callback();
            }

        });
    }

}
