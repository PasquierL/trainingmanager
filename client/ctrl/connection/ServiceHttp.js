/**
 * @file Envoie les requetes au serveur
 * @author Louis Pasquier
 */
class ServiceHttp {

    BASE_URL;

    constructor() {
        this.BASE_URL = "http://pasquierl.emf-informatique.ch/trainingmanager/server/";
        //this.BASE_URL = "http://localhost/trainingManager/server/";
    }

    /**
     * lancement de la connexion
     *
     * @param username
     * @param password
     * @param successCallback
     * @param errorCallback
     */
    checkLogin(username, password, successCallback, errorCallback) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.BASE_URL + "loginManager.php",
            data:'action=connect&username=' + username + '&password=' + password,
            success: successCallback,
            error: errorCallback
        });
    }

    /**
     * Déconnexion du compte actuellement connecté
     *
     * @param successCallback
     * @param errorCallback
     */
    disconnect(successCallback, errorCallback) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: this.BASE_URL + "loginManager.php",
            data:'action=disconnect',
            success: successCallback,
            error: errorCallback
        });
    }

    /**
     * Vérifie si le compte connecté est admin
     *
     * @param successCallback
     * @param errorCallback
     */
    isAdmin(successCallback, errorCallback) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: this.BASE_URL + "loginManager.php",
            data:'action=isAdmin',
            success: successCallback,
            error: errorCallback
        });
    }

    /**
     * Ajout d'un utilisateur dans la DB
     *
     * @param username
     * @param password
     * @param successCallback
     * @param errorCallback
     */
    register(username, password, errorCallback) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.BASE_URL + "loginManager.php",
            data:'action=add&username=' + username + '&password=' + password + '&isAdmin=' + 0,
            error: errorCallback
        });
    }

    /**
     * Ajout d'une session d'entrainement
     *
     * @param date
     * @param successCallback
     * @param errorCallback
     */
    addSession(date, successCallback, errorCallback) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.BASE_URL + "trainingManager.php",
            data:'action=addSession&date=' + date,
            success: successCallback,
            error: errorCallback
        });
    }

    /**
     * Ajout d'un exercice réalisé une certaine semaine avec les poids soulevés
     *
     * @param exercice
     * @param semaine
     * @param poidgauche
     * @param poiddroite
     * @param successCallback
     * @param errorCallback
     */
    addExerciceSemaine(exercice, semaine, poidgauche, poiddroite, successCallback, errorCallback) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.BASE_URL + "trainingManager.php",
            data:'action=addExerciceSemaine&exercice=' + exercice + '&semaine=' + semaine + '&poidgauche=' + poidgauche + '&poiddroite=' + poiddroite,
            success: successCallback,
            error: errorCallback
        });
    }

    /**
     * Chargement des sessions d'entrainement
     *
     * @param successCallback
     * @param errorCallback
     */
    loadSessions(successCallback, errorCallback) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: this.BASE_URL + "trainingManager.php",
            data:'action=getSessions',
            success: successCallback,
            error: errorCallback
        });
    }

    /**
     * Chargement des exercices avec la semaine ou ils ont été réalisés
     *
     * @param successCallback
     * @param errorCallback
     */
    loadExerciceSemaine(successCallback, errorCallback) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: this.BASE_URL + "trainingManager.php",
            data:'action=getExerciceSemaine',
            success: successCallback,
            error: errorCallback
        });
    }

}

