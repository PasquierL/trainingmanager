/**
 * @file Controller principale
 * @author Louis Pasquier
 */

$().ready(function () {
    serviceJs = new ServiceJs();
    serviceHttp = new ServiceHttp();
    indexCtrl = new IndexCtrl();
    serviceJs.centraliserErreurHttp(IndexCtrl.afficherErreurHttp);
});

class IndexCtrl {

    constructor() {
        this.loadLogin();
    }

    /**
     * Affichage d'erreurs
     *
     * @param msg
     */
    static afficherErreurHttp(msg) {
        console.log(msg)
        alert(msg);
    }

    /**
     * Chargement de la page de login
     */
    loadLogin() {
        serviceJs.chargerVue("login");
    }

    /**
     * Chargement de la page des ajout de sessions
     */
    loadAddSession() {
        serviceJs.chargerVue("addSession");
    }

    /**
     * Chargement de la page qui affiche les tableaux
     */
    loadGetSession() {

        serviceJs.chargerVue("getSessions");

        $().ready(() => {
            serviceHttp.loadSessions((data) => LoadSessionCtrl.fillSessionTab(data), IndexCtrl.afficherErreurHttp);
        });

    }

    /**
     * Charge la vue pour le diagramme
     */
    loadRecapSession() {
        serviceJs.chargerVue("recapSessions");
    }

    /**
     * Charge la vue pour toutes les sessions de tous les users
     */
    loadUsersSession() {
        serviceJs.chargerVue("getSessionsUsers");
    }

    /**
     * Connecte l'utilisateur
     */
    login() {
        if (!LoginCtrl.areCredentialsValid()) return;

        let username = $('#username').val();
        let password = $('#password').val();

        IndexCtrl.loader();

        setTimeout(() => {
            //connect user
            serviceHttp.checkLogin(
                username, password,
                this.connectSuccess, IndexCtrl.afficherErreurHttp);
        }, 1500);

    }

    /**
     * Petit loader sympa
     */
    static loader() {
        serviceJs.chargerVue("spinner");
    }

    /**
     * Déconnecte l'utilisateur
     */
    disconnect() {
        serviceHttp.disconnect(() => {
            this.loadLogin();
            serviceJs.chargerMenu("emptyMenu");
        }, IndexCtrl.afficherErreurHttp)
    }

    /**
     * Créé un nouvel utilisateur
     */
    register() {
        if (!LoginCtrl.areCredentialsValid()) return;

        serviceHttp.register($('#username').val(), $('#password').val(), IndexCtrl.afficherErreurHttp);
        this.login();
    }

    /**
     * Verifie avec le resultat de la requete si le login à fonctionné
     * On vérifie aussi si le user est admin pour charger le bon menu
     *
     * @param data
     */
    connectSuccess(data) {
        if (data.result === "true") {
            console.log("Login ok");
            //check admin
            serviceHttp.isAdmin((data) => {
                if (data.result === "true") {
                    serviceJs.chargerMenu("adminMenu");
                } else {
                    serviceJs.chargerMenu("baseMenu");
                }
                indexCtrl.loadGetSession();
            }, IndexCtrl.afficherErreurHttp);
        } else {
            alert("Erreur lors du login !")
            indexCtrl.loadLogin();
        }
    }
}