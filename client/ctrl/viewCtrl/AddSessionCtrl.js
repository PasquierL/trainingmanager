/**
 * @file Controller de l'ajout de session
 * @author Louis Pasquier
 */
class AddSessionCtrl {

    constructor() {}

    /**
     * Ajout d'une session
     */
    static addSession() {
        let date;
        if ((date = $('#date').val()) === null) {
            window.alert("La date entrée n'est pas correcte !");
            return;
        }
        let semaine = $('#semaine').val();
        if ( (semaine === null) || (semaine === "") || (semaine < 0)) {
            window.alert("La semaine entrée n'est pas correcte !");
            return;
        }

        //session
        serviceHttp.addSession(date, AddSessionCtrl.addSessionSuccess, AddSessionCtrl.addSessionError);

    }

    /**
     * Si la session à été ajoutée
     */
    static addSessionSuccess() {
        AddSessionCtrl.addExerciceSemaine();
        window.alert("La session à été ajoutée !!");
    }

    /**
     * Si il y a eut une erreure lors de l'ajout
     */
    static addSessionError(){
        IndexCtrl.afficherErreurHttp("Erreur lors de l'ajout de la session");
    }

    /**
     * Ajout d'un exercice selon une certaine semaine
     *
     */
    static addExerciceSemaine() {
        //1erephalange
        let val1erephalange = $('#add1erephalange').val();
        if ( (val1erephalange !== "") && (val1erephalange !== null) && (val1erephalange !== undefined)) {
            serviceHttp.addExerciceSemaine("Suspension 1ere phalange", $('#semaine').val(),
                val1erephalange, 0,
                () => {
                    console.log("exercice 1ere phalange ajouté");
                },IndexCtrl.afficherErreurHttp);
        } else {
            window.alert("Valeur d'exercice pas valide !!");
        }

        //2emephalange
        let val2emephalange = $('#add2emephalange').val();
        if ( (val2emephalange !== "") && (val2emephalange !== null) && (val2emephalange !== undefined)) {
            serviceHttp.addExerciceSemaine("Suspension 2eme phalange", $('#semaine').val(),
                val2emephalange, 0,
                () => {
                    console.log("exercice 2eme phalange ajouté")
                },IndexCtrl.afficherErreurHttp);
        } else {
            window.alert("Valeur d'exercice pas valide !!");
        }

        //boule
        let valboule = $('#addboule').val();
        if ( (valboule !== "") && (valboule !== null) && (valboule !== undefined) ) {
            serviceHttp.addExerciceSemaine("Suspension boules", $('#semaine').val(),
                valboule, 0,
                () => {
                    console.log("exercice boule ajouté")
                },IndexCtrl.afficherErreurHttp);
        } else {
            window.alert("Valeur d'exercice pas valide !!");
        }

        //arque
        let valarque = $('#addarque').val();
        if ( (valarque !== "") && (valarque !== null) && (valarque !== undefined)) {
            serviceHttp.addExerciceSemaine("Suspension arques", $('#semaine').val(),
                valarque, 0,
                () => {
                    console.log("exercice arque ajouté")
                },IndexCtrl.afficherErreurHttp);
        } else {
            window.alert("Valeur d'exercice pas valide !!");
        }

    }
}