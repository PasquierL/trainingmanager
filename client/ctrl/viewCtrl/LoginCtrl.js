/**
 * @file Controller du login
 * @author Louis Pasquier
 */
class LoginCtrl {

    constructor() {}

    /**
     * Vérifie si les champs ne sont pas vides
     *
     * @returns {boolean}
     */
    static areCredentialsValid() {
        if (document.getElementById("username").value == null) return false;
        if (document.getElementById("username").value === "") return false;
        if (document.getElementById("password").value == null) return false;
        if (document.getElementById("password").value === "") return false;

        return true;
    }
}
