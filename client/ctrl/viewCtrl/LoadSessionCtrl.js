/**
 * @file Controller du chargement de session
 * @author Louis Pasquier
 */
class LoadSessionCtrl {

    constructor() {}

    /**
     * Charge les données dans un tableau sur la page html
     *
     * @param data données retournées par la requete
     */
    static fillSessionTab(data) {

        if ( (data === undefined) || (data === null) || (data[0] === undefined)) return;

        console.log(data);

        let j = 0;
        for (let i = 0; i < data.length; i++) {

            if ((data[i] === undefined) || (data[i] === null)) continue; //si il n'y a rien on passe au suivant

            switch (data[i].Nom) {
                case "Suspension 1ere phalange":
                    //ajout d'un colonne pour la date
                    $('#getExerciceTabTitle').append('<th>'+  data[j].Date +'</th>');
                    j += 4;
                    $('#get1erephalange').append('<td>'+ data[i].PoidDroite +' : '+ data[i].PoidGauche +'</td>');
                    break;
                case "Suspension 2eme phalange":
                    $('#get2emephalange').append('<td>'+ data[i].PoidDroite +' : '+ data[i].PoidGauche +'</td>');
                    break;
                case "Suspension boules":
                    $('#getboule').append('<td>'+ data[i].PoidDroite +' : '+ data[i].PoidGauche +'</td>');
                    break;
                case "Suspension arques":
                    $('#getarque').append('<td>'+ data[i].PoidDroite +' : '+ data[i].PoidGauche +'</td>');
                    break;
            }

        }
    }

}