/**
 * @file Controller du récapitulatif des sessions
 * @author Louis Pasquier
 */
class RecapSessionCtrl {

    constructor() {}

    /**
     * Chargement des donnée
     */
    static loadData() {
        serviceHttp.loadExerciceSemaine((values) => RecapSessionCtrl.buildDiagram(values), IndexCtrl.afficherErreurHttp);
    }

    /**
     * Création du diagramme (avec api Chart.js)
     *
     * @param requestValues
     */
    static buildDiagram(requestValues) {

        if ( (requestValues === undefined) || (requestValues[0] === undefined)) return;

        let week = [requestValues[0].Semaine];
        let exercice = [];
        let colors = ['rgb(54, 162, 235)', 'rgb(213, 53, 37)', 'rgb(78,235,54)', 'rgb(241,202,88)'
            , 'rgb(211,54,235)', 'rgb(54,235,202)', 'rgb(235,145,54)'];

        for (let i = 0; i < requestValues.length; i++) {

            //on remplit un tableau avec toutes les semaines
            if (week[week.length -1] !== requestValues[i].Semaine) {
                week.push(requestValues[i].Semaine);
            }

            let index = RecapSessionCtrl.arrayContain(exercice, requestValues[i].Nom);

            //if (exercice.includes(requestValues[i].Nom)) {
            if (index !== -1) {
                //exercice already in the tab
                exercice[index].push(requestValues[i].PoidGauche);
            } else {
                //need to add exercice to the tab
                exercice.push([requestValues[i].Nom, requestValues[i].PoidGauche]);
            }

        }

        const data = {
            labels: week,
            datasets: []
        };

        for (let i = 0; i < exercice.length; i++) {

            let exerciceValues = [];
            for (let j = 1; j < exercice[i].length; j++) {
                exerciceValues.push(exercice[i][j]);
            }

            data.datasets.push({
                label: exercice[i][0],
                borderColor: colors[i],
                backgroundColor: colors[i],
                data: exerciceValues,
            });
        }

        new Chart($('#recapSession'), {
            type: 'line',
            data: data,
            options: {}
        });

    }

    /**
     * cherche l'index d'un element dans un tableau
     *
     * @param array
     * @param element
     * @returns {number}
     */
    static arrayContain(array, element) {
        for (let i = 0; i < array.length; i++) {
            if (array[i][0] === element ) {
                return i;
            }
        }
        return -1;
    }

}